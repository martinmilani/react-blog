import { createSlice } from "@reduxjs/toolkit";

export const postsSlice = createSlice({
  name: "posts",
  initialState: {
    posts: [],
  },
  reducers: {
    loadPosts: (state, action) => {
      state.posts = action.payload;
    },

    deletePost: (state, action) => {
      let id = action.payload;
      let idNumber = parseInt(id);
      let index = state.posts.map((post) => post.id).indexOf(idNumber);
      let a = state.posts.slice(0, index);
      let b = state.posts.slice(index + 1);
      state.posts = a.concat(b);
    },

    createPost: (state, action) => {
      let lastIndex = state.posts.length - 1;
      let lastId = state.posts[lastIndex].id;
      let newId = lastId + 1;
      let newPost = [
        {
          id: newId,
          title: action.payload.title,
          body: action.payload.body,
        },
      ];
      state.posts = state.posts.concat(newPost);
    },

    editPost: (state, action) => {
      let id = action.payload.id;
      let idNumber = parseInt(id);
      let index = state.posts.map((post) => post.id).indexOf(idNumber);
      let newArray = [...state.posts];
      let newPost = {
        id: idNumber,
        title: action.payload.title,
        body: action.payload.body,
      };
      newArray[index] = newPost;
      state.posts = newArray;
    },
  },
});

// Action creators are generated for each case reducer function
export const { loadPosts, deletePost, createPost, editPost } =
  postsSlice.actions;

export default postsSlice.reducer;
