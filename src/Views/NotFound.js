import React from "react";
import { Link } from "react-router-dom";

function NotFound() {
  return (
    <div className="vw-100 vh-100 d-flex justify-content-center align-items-center text-center">
      <div>
        <h1>
          <strong>Oops!</strong> Page not Found!
        </h1>
        <Link to="/">Go Home</Link>
      </div>
    </div>
  );
}

export default NotFound;
