import React from "react";
import { Redirect } from "react-router-dom";
import EditForm from "../Components/Edit/EditForm";

function Edit({ authorized }) {
  if (!authorized) {
    return <Redirect to="login" />;
  }
  return (
    <div>
      <EditForm />
    </div>
  );
}

export default Edit;
