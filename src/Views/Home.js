import React from "react";
import { Redirect } from "react-router-dom";
import PostList from "../Components/Home/PostList";

function Home({ authorized }) {
  if (!authorized) {
    return <Redirect to="login" />;
  }
  return (
    <div>
      <PostList />
    </div>
  );
}

export default Home;
