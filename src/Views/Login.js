import React from "react";
import LoginForm from "../Components/LoginForm";
import Container from "react-bootstrap/Container";

function Login() {
  return (
    <Container fluid>
      <main className="vh-100 d-flex flex-column justify-content-center align-items-center">
        <LoginForm />
      </main>
    </Container>
  );
}

export default Login;
