import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";

import Home from "./Views/Home";
import Login from "./Views/Login";
import Edit from "./Views/Edit";
import NavBar from "./Components/NavBar";
import NotFound from "./Views/NotFound";

function App() {
  return (
    <div className="app">
      <Router>
        <Switch>
          <Route
            exact
            path="/"
            render={() => {
              return window.localStorage.getItem("token") ? (
                <Redirect to="/home" />
              ) : (
                <Redirect to="/login" />
              );
            }}
          />
          <Route exact path="/login" component={Login}></Route>

          <Route
            exact
            path="/home"
            component={() => (
              <>
                <NavBar />
                <Home authorized={window.localStorage.getItem("token")} />
              </>
            )}
          ></Route>

          <Route
            exact
            path="/edit/:id"
            component={() => (
              <>
                <NavBar />
                <Edit authorized={window.localStorage.getItem("token")} />
              </>
            )}
          ></Route>
          <Route path="*" component={NotFound}></Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
