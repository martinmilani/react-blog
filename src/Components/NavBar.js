import React from "react";
import { useHistory, Link } from "react-router-dom";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";

function Header() {
  let history = useHistory();

  const logout = () => {
    try {
      window.localStorage.removeItem("token");
      history.push("/login");
    } catch (error) {
      alert(error);
    }
  };

  return (
    <Navbar bg="light" expand="lg">
      <Nav className="d-flex flex-row justify-content-between w-100">
        <div className="d-flex flex-row align-items-center">
          <Link to="/home" className="text-secondary">
            Home
          </Link>
        </div>
        <div className="d-flex flex-row align-items-center">
          <Link to="/edit/select-post" className="mr-4 text-secondary">
            Edit Post
          </Link>
          <Nav.Link onClick={logout}>Logout</Nav.Link>
        </div>
      </Nav>
    </Navbar>
  );
}

export default Header;
