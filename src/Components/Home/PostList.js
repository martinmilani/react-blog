import React, { useEffect, useState } from "react";
import Container from "react-bootstrap/Container";
import Button from "react-bootstrap/Button";
import PostCard from "./PostCard";
import Axios from "axios";
import SpinerLoading from "../SpinerLoading";
import { useDispatch, useSelector } from "react-redux";
import { loadPosts } from "../../redux/posts";
import { Link } from "react-router-dom";

function PostList() {
  const { posts } = useSelector((state) => state.posts);
  const dispatch = useDispatch();
  const [isLoaded, setIsLoaded] = useState(false);

  useEffect(() => {
    let empty = posts.length === 0;
    if (empty) {
      let fetchData = async () => {
        let url = "https://jsonplaceholder.typicode.com/posts";
        try {
          let res = await Axios.get(url);
          let data = res.data;
          let postArr = data.slice(0, 10);
          dispatch(loadPosts(postArr));
          setIsLoaded(true);
        } catch (error) {
          setIsLoaded(true);
          alert(error);
        }
      };
      fetchData();
    } else {
      setIsLoaded(true);
    }
  }, [dispatch, posts]);

  return (
    <Container className="mt-5 d-flex flex-column justify-content-center">
      <div className="d-flex justify-content-center mb-4">
        <Link
          to="/edit/new-post"
          className="d-flex justify-content-center w-100 text-decoration-none"
        >
          <Button
            variant="outline-primary"
          >
            Crear Nuevo Post
          </Button>
        </Link>
      </div>
      <h3 className="text-center mb-4 mt-md-4">Post List</h3>
      {!isLoaded ? (
        <SpinerLoading />
      ) : (
        <div className="d-flex justify-content-center mt-md-4">
          <ul className="list-unstyled">
            {posts.map((post) => (
              <li key={post.id}>
                <PostCard title={post.title} id={post.id} body={post.body} />
              </li>
            ))}
          </ul>
        </div>
      )}
    </Container>
  );
}

export default PostList;
