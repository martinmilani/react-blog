import React from "react";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import Axios from "axios";
import { Accordion } from "react-bootstrap";
import SpinerLoading from "../SpinerLoading";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { deletePost } from "../../redux/posts";
import { Link } from "react-router-dom";

function PostCard({ title, id, body }) {
  const [isPostBodyLoaded, setIsPostBodyLoaded] = useState(false);
  const dispatch = useDispatch();

  const deletePostApi = async (id) => {
    try {
      await Axios.delete(`https://jsonplaceholder.typicode.com/posts/${id}`);
      dispatch(deletePost(id));
    } catch (error) {
      alert(error);
    }
  };

  const getPostBody = async (id) => {
    try {
      await Axios.get(`https://jsonplaceholder.typicode.com/posts/${id}`);
      setIsPostBodyLoaded(true);
    } catch (error) {
      alert(error);
      setIsPostBodyLoaded(true);
    }
  };

  const handleMore = (e) => {
    let postId = e.target.value;
    getPostBody(postId);
  };

  const handleDelete = (e) => {
    let postId = e.target.value;
    deletePostApi(postId);
  };

  return (
    <div>
      <Accordion>
        <Card className="mb-4" style={{ maxWidth: "700px" }}>
          <Card.Title className="text-capitalize px-3 pt-3 pb-1 mb-0">
            {title}
          </Card.Title>
          <Card.Body className="py-2">
            <Accordion.Collapse eventKey="0">
              <Card.Body className="w-100 p-0">
                {!isPostBodyLoaded ? (
                  <SpinerLoading />
                ) : (
                  <p className="p-0">{body}</p>
                )}
              </Card.Body>
            </Accordion.Collapse>
            <div className="d-flex justify-content-end">
              <Button
                onClick={handleDelete}
                variant="link"
                size="sm"
                value={id}
              >
                Delete
              </Button>
              <Button variant="link" size="sm">
                <Link to={`/edit/${id}`}>Edit</Link>
              </Button>
              <Accordion.Toggle
                as={Button}
                variant="link"
                eventKey="0"
                size="sm"
                className="text-decoration-none"
                onClick={handleMore}
                value={id}
              >
                More
              </Accordion.Toggle>
            </div>
          </Card.Body>
        </Card>
      </Accordion>
    </div>
  );
}

export default PostCard;
