import React, { useEffect, useState } from "react";
import { Formik, Form, Field } from "formik";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { useDispatch, useSelector } from "react-redux";
import { createPost, editPost } from "../../redux/posts";
import { useParams } from "react-router-dom";
import SpinerLoading from "../SpinerLoading";
import SelectPost from "./SelectPost";
import Axios from "axios";

function EditForm() {
  const { posts } = useSelector((state) => state.posts);
  const dispatch = useDispatch();
  const { id } = useParams();
  const [selectId, setSelectId] = useState(id);
  const [postTitle, setPostTitle] = useState("");
  const [postBody, setPostBody] = useState("");
  const [isLoaded, setIsLoaded] = useState(
    id === "new-post" || id === "select-post"
  );

  const havePostId = selectId !== "new-post" && selectId !== "select-post";

  const handleOnChange = (value) => {
    setSelectId(value);
  };

  useEffect(() => {
    if (havePostId) {
      let fetchData = async () => {
        let idNumber = parseInt(selectId);
        let index = posts.map((post) => post.id).indexOf(idNumber);
        let url = `https://jsonplaceholder.typicode.com/posts/${selectId}`;
        try {
          await Axios.get(url);
          setPostTitle(posts[index].title);
          setPostBody(posts[index].body);
        } catch (error) {
          alert(error);
        }
        setIsLoaded(true);
      };
      fetchData();
    }
  }, [selectId, havePostId]);

  const createNewPost = async (values, resetForm) => {
    let url = "https://jsonplaceholder.typicode.com/posts";
    let title = values.title;
    let body = values.body;
    try {
      await Axios.post(url, { title, body });
      dispatch(createPost({ title: title, body: body }));
      alert("New post created!!!");
    } catch (error) {
      alert(error);
    }
    resetForm();
  };

  const modifyPost = async (values, slectId, resetForm) => {
    let url = `https://jsonplaceholder.typicode.com/posts/${selectId}`;
    let id = selectId;
    let title = values.title;
    let body = values.body;
    try {
      await Axios.patch(url, { id, title, body });
      dispatch(editPost({ id: id, title: title, body: body }));
      alert("Post Edited!!!");
    } catch (error) {
      alert(error);
    }
    setPostTitle("");
    setPostBody("");
    resetForm();
  };

  return (
    <>
      {id === "select-post" ? (
        <SelectPost handleOnChange={handleOnChange} />
      ) : null}

      {!isLoaded ? (
        <SpinerLoading />
      ) : (
        <Formik
          enableReinitialize={true}
          initialValues={{
            title: postTitle,
            body: postBody,
          }}
          validate={(values) => {
            const errors = {};
            if (!values.title) {
              errors.title = "A post title is required";
            }
            if (!values.body) {
              errors.body = "A post text is required";
            }
            return errors;
          }}
          onSubmit={(values, { resetForm }) => {
            if (id !== "new-post") {
              return modifyPost(values, id, resetForm);
            } else {
              return createNewPost(values, resetForm);
            }
          }}
        >
          {({ errors, touched, isSubmitting, handleChange, values }) => (
            <Row className="w-100 justify-content-center m-0">
              <Col sm={12} md={8} lg={4}>
                <Form className="mt-2 mb-4">
                  <h1 className="h3 pt-3 my-4 fw-normal text-center">
                    {selectId !== "new-post" ? "Edit " : "Create "}Post
                  </h1>
                  <div className="form-floating mb-4 pt-4">
                    <Field
                      values={values.title}
                      onChange={handleChange}
                      type="text"
                      name="title"
                      className={
                        "form-control " +
                        (errors.title && touched.title ? "is-invalid" : null)
                      }
                      placeholder="Title"
                    />
                    <div className="invalid-feedback">{errors.title}</div>
                  </div>

                  <div className="form-floating mb-5 w-100">
                    <Field
                      values={values.body}
                      onChange={handleChange}
                      type="text"
                      name="body"
                      as="textarea"
                      style={{ height: "150px" }}
                      className={
                        "form-control " +
                        (errors.body && touched.body ? "is-invalid" : null)
                      }
                      placeholder="Post text"
                    />

                    <div className="invalid-feedback">{errors.body}</div>
                  </div>

                  {isSubmitting ? (
                    <button
                      className="w-100 btn btn-lg btn-primary d-flex justify-content-center align-items-center"
                      type="button"
                      disabled
                    >
                      <span
                        className="spinner-border spinner-border-sm mr-2"
                        role="status"
                        aria-hidden="true"
                      ></span>
                      Loading...
                    </button>
                  ) : (
                    <button className="btn w-100 btn-primary" type="submit">
                      <h4 className='m-0 p-0'>Submit</h4>
                    </button>
                  )}
                </Form>
              </Col>
            </Row>
          )}
        </Formik>
      )}
    </>
  );
}

export default EditForm;
