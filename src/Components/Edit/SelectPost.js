import React from "react";
import Form from "react-bootstrap/Form";
import { Row, Col } from "react-bootstrap";
import { useSelector } from "react-redux";

function SelectPost({ handleOnChange }) {
  const { posts } = useSelector((state) => state.posts);

  return (
    <Row className="w-100 justify-content-center m-0 mt-2 pt-5">
      <Col sm={12} md={8} lg={4}>
        <Form.Label className="d-flex justify-content-center">
          Select a post to edit
        </Form.Label>
        <Form.Control
          as="select"
          onChange={(e) => handleOnChange(e.target.value)}
        >
          {posts.map((post) => (
            <option key={post.id} value={post.id}>
              {post.title}
            </option>
          ))}
        </Form.Control>
      </Col>
    </Row>
  );
}

export default SelectPost;
