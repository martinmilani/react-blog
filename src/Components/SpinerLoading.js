import React from "react";

function SpinerLoading() {
  return (
    <div className="d-flex flex-row justify-content-center align-items-center m-2 p-2">
      <div className="spinner-grow text-primary" role="status">
        <span className="visually-hidden"></span>
      </div>

      <div className="text-center mt-2 ml-2">
        <h4>Loading...</h4>
      </div>
    </div>
  );
}

export default SpinerLoading;
